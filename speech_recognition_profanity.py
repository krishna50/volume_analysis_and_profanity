#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jun 16 18:39:43 2020

@author: krishna
"""

import os
import json
import sox
from profanity_check import predict_prob

def create_manifest(audio_filepath):
    create_dict = {}
    create_dict["audio_filepath"] =audio_filepath
    create_dict["duration"] = 5.6
    create_dict["text"] = " "
    with open('manifest.infer','w') as fobj:
        json.dump(create_dict,fobj) 
        print('Manifest file is created')
        

def call_asr(cur_dir,asr_path):
    audio_filepath = os.getcwd()+'/temp.wav'
    os.chdir(asr_path)
    create_manifest(audio_filepath)
    run_code = 'python2 decoding.py'
    os.system(run_code)
    read_text = [line.rstrip('\n') for line in open('temp.txt')]
    os.chdir(cur_dir)
    return read_text
    

def process_speech_recognition(segmentation_json,audio_filepath,asr_path):
    full_dataframe=[]
    for item in segmentation_json:
        start_time = item['start_time']
        end_time = item['end_time']
        tfm = sox.Transformer()
        tfm.trim(start_time,end_time)
        tfm.build(audio_filepath, 'temp.wav')
        cut_seg_path = 'temp.wav'
        cur_dir =os.getcwd()
        text_data = call_asr(cur_dir,asr_path)
        try:
            text = text_data[0]
        except:
            text='NULL'
        profanity_score = predict_prob([text])
        data_frame = {}
        data_frame['start_time'] = start_time
        data_frame['end_time'] = end_time
        data_frame['transcripts'] = text
        data_frame['profanity_score']=profanity_score
        full_dataframe.append(data_frame)
        os.remove(cut_seg_path)
    return full_dataframe




def combine_profanity(json_lists,max_duration=1200):
    create_full_list=[]
    p=0
    for json_list in json_lists:
        for item in json_list:
            start_time=item['start_time']+p*max_duration
            end_time = item['end_time']+p*max_duration
            text = item['transcripts']
            score = item['profanity_score']
            create_dict={}
            create_dict['start_time'] = start_time
            create_dict['end_time'] = end_time
            create_dict['transcripts'] = text
            create_dict['profanity_score'] = str(score)
            create_full_list.append(create_dict)
        p=p+1
    return create_full_list
