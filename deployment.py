#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jun 16 12:02:20 2020

@author: krishna
"""

import sox
import os
import librosa
import datetime
import csv
import json 
import torch.nn as nn
import os
import argparse
import torch
import numpy as np
from torch import optim
import librosa
from sklearn.metrics import accuracy_score
import glob
from shutil import rmtree
import torch.nn.functional as F
from shutil import copyfile
import sys
from utils.utility import extract_audio
from utils.utility import downsample
from utils.utility import chunk_big_files
from inaSpeechSegmenter import Segmenter, seg2csv
from utils.utility import parse_speech_segments
from volume_analysis import process_volume_analysis,combine_volume_analysis
from speech_recognition_profanity import process_speech_recognition,combine_profanity

#from create_srt import create_srt


def data_prep(video_filepath,dump_dir,max_duration=1200):
    
    if not os.path.exists(dump_dir):
        os.makedirs(dump_dir)
    create_video_dump_dir = os.path.join(dump_dir,'_'.join(video_filepath.split('/')[-1][:-4].split(' ')))
    new_video_loc = os.path.join(create_video_dump_dir,'_'.join(video_filepath.split('/')[-1].split(' ')))
    
    try:
        os.makedirs(create_video_dump_dir)
    except:
        print('Folder exist! Please check the video if it is already processed :D')
        print('Exciting the code!!! Delete the folder and rerun the code')
        sys.exit()
        
    copyfile(video_filepath,new_video_loc)
    
    print('Chunking the audio into 20 minutes segments')
    segment_folder=chunk_big_files(new_video_loc,create_video_dump_dir,max_duration) 
    return segment_folder,create_video_dump_dir







def run_segmentation(audio_filepath_16k):
    seg = Segmenter()
    segmentation = seg(audio_filepath_16k)
    json_ret = parse_speech_segments(segmentation)
    return json_ret



def run_volume_analysis(seg_json_data,audio_filepath_16k):
    volume_json  = process_volume_analysis(seg_json_data,audio_filepath_16k)
    return volume_json

def run_speech_recognition_profanity(seg_json_data,audio_filepath_16k,asr_path):
    asr_profanity = process_speech_recognition(seg_json_data,audio_filepath_16k,asr_path)
    return asr_profanity


def volume_analysis(args):
    segment_folder, video_folder = data_prep(args.video_filepath,args.dump_dir,max_duration=1200)
    all_segments = sorted(glob.glob(segment_folder+'/*.mp4'))
    
    ##### Volume_analysis
    json_lists = []
    for segment_path in all_segments:
        audio_filepath = extract_audio(segment_path)
        audio_filepath_16k = downsample(audio_filepath)
        seg_json_data = run_segmentation(audio_filepath_16k)
        volume_json = run_volume_analysis(seg_json_data,audio_filepath_16k)
        json_lists.append(volume_json)
        os.remove(audio_filepath)
        os.remove(audio_filepath_16k)
    volume_analysis_final = combine_volume_analysis(json_lists)
    save_path_vol_res = args.video_filepath[:-4]+'_volume_analysis.json'
    with open(save_path_vol_res,'w') as fobj:
        json.dump(volume_analysis_final,fobj,indent=4) 
    rmtree(video_folder)
    ######## Profanity

    

def profanity(args):
    segment_folder, video_folder = data_prep(args.video_filepath,args.dump_dir,max_duration=1200)
    all_segments = sorted(glob.glob(segment_folder+'/*.mp4'))
    ##### Volume_analysis
    json_lists = []
    for segment_path in all_segments:
        audio_filepath = extract_audio(segment_path)
        audio_filepath_16k = downsample(audio_filepath)
        seg_json_data = run_segmentation(audio_filepath_16k)
        profanity_json = run_speech_recognition_profanity(seg_json_data,audio_filepath_16k,args.asr_path)
        json_lists.append(profanity_json)
        os.remove(audio_filepath)
        os.remove(audio_filepath_16k)
    profanity_final = combine_profanity(json_lists)
    save_path_prof_res = args.video_filepath[:-4]+'_profanity.json'
    with open(save_path_prof_res,'w') as fobj:
        json.dump(profanity_final,fobj,indent=4) 
    rmtree(video_folder)
    

if __name__ == '__main__':
    parser = argparse.ArgumentParser(add_help=False)
    parser.add_argument('--video_filepath',type=str,default='/home/krishna/Desktop/COD_testing_videos/chocoTaco Goes on a Killing Spree ft. chun and Boom - COD Warzone Gameplay-P9xJCtMsUVI.mp4')
    parser.add_argument('--asr_path',type=str, default='/home/krishna/Krishna/DeepSpeech_youplus/DeepSpeech')
    parser.add_argument('--dump_dir', type=str, default='/media/newhd/sizzle_dump/')
    parser.add_argument('--save_results', type=bool, default=True)
    
    args = parser.parse_args()
    print('Running Volume analysis model')
    volume_analysis(args)
    print('Finished Volume analysis')
    profanity(args)

