# Volume_analysis_and_profanity

This project contains code for volume analysis, profanity and speech recognition

# Installation


## Installing DeepSpeech package
This package has to be installed on python2
Please follow the this installation instruction provided in the following section.

Install the following libraries
```
sudo apt-get install -y pkg-config libflac-dev libogg-dev libvorbis-dev libboost-dev swig python-dev ffmpeg

```

Once the above packages are installed succesfully, install the python packages

```
pip2 install paddlepaddle
git clone https://github.com/PaddlePaddle/DeepSpeech.git
cd DeepSpeech
sh setup.sh
```

## Installing volume analysis and profanity
Note: This code runs only on python3
```bash
git clone https://gitlab.com/krishna50/volume_analysis_and_profanity.git
```
Once you install python3 successfully, install required packges using requirements.txt
```bash
pip3 install -r requirements.txt
```

## Copying models and codes for speech recognitino
Download the acoustic and langauge model from Sizzle Server at this location -- /mnt/archive/speech_pipeline/AC_LM.zip

```
unzip AC_LM.zip
mv AC_LM DeepSpeech/models/
mv decoding.py DeepSpeech/
mv manifest.infer DeepSpeech/
```

## Running Volume analysis and Profanity models
```
python3 deployment.py --video_filepath $video_file --asr_path $DeepSpeech_installation_path
                      --dump_dir $some_random_dir  --save_results True
                      
```
For example
```
python3 deployment.py --video_filepath /home/krishna/Desktop/COD_testing_videos/Ninja.mp4 --asr_path /home/krishna/Krishna/DeepSpeech/
                      --dump_dir /media/newhd/sizzle_dump  --save_results True
```


 