#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jun 16 12:53:48 2020

@author: krishna
"""

import sox
import librosa
import numpy as np
import os
import glob

def compute_energy(filename):
    audio_data, sr = librosa.load(filename,sr=16000)
    energy_contour = librosa.feature.rms(audio_data,frame_length=3200,hop_length=1600)
    return energy_contour


def process_volume_analysis(segmentation_json,audio_filepath):
    full_dataframe=[]
    for item in segmentation_json:
        start_time = item['start_time']
        end_time = item['end_time']
    
        tfm = sox.Transformer()
        tfm.trim(start_time,end_time)
        tfm.build(audio_filepath, 'temp.wav')
        cut_seg_path = 'temp.wav'
        energy_values = compute_energy(cut_seg_path)
        find_max = np.max(energy_values)
        data_frame = {}
        data_frame['start_time'] = start_time
        data_frame['end_time'] = end_time
        data_frame['score'] = find_max
        full_dataframe.append(data_frame)
        os.remove(cut_seg_path)
    return full_dataframe


def combine_volume_analysis(json_lists,max_duration=1200):
    create_full_list=[]
    p=0
    for json_list in json_lists:
        for item in json_list:
            start_time=item['start_time']+p*max_duration
            end_time = item['end_time']+p*max_duration
            label = item['score']
            create_dict={}
            create_dict['start_time'] = start_time
            create_dict['end_time'] = end_time
            create_dict['volume_level'] = str(label)
            create_full_list.append(create_dict)
        p=p+1
    return create_full_list
