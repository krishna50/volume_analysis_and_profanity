#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jun 11 18:11:11 2020

@author: krishna
"""

import glob
import os
import numpy as np
import soundfile as sf
from shutil import copyfile


def extract_audio(video_filepath):
    #dump_audio_path = os.path.join(dump_dir,video_filepath.split('/')[-1][:-4]+'.wav')
    extract_audio = 'ffmpeg -i '+video_filepath+' -f wav '+video_filepath[:-4]+'.wav'
    try:
        os.system(extract_audio)
        print('Extracted audio succesfully for {} '.format(video_filepath))
        dump_audio_path = video_filepath[:-4]+'.wav'
        return dump_audio_path
    except:
        print('Not able to extract due to some errors')
        


def downsample(audio_filepath):
    downsample = 'sox  '+audio_filepath+' -r 16k -c 1  '+audio_filepath[:-4]+'_16k.wav'
    try:
        os.system(downsample)
        print('Successfully downsampled {}'.format(audio_filepath))
        return audio_filepath[:-4]+'_16k.wav'
    except:
        print('Not able to downsample due to some errors')
        

def chunk_big_files(video_filepath,create_video_dump_dir,max_duration=1200):
    segment_folder = os.path.join(create_video_dump_dir,'segments')
    if not os.path.exists(segment_folder):
        os.makedirs(segment_folder)
    
    #'ffmpeg -i input.mp4 -c copy -map 0 -segment_time 00:20:00 -f segment -reset_timestamps 1 output%03d.mp4'
    segment_audio='ffmpeg -i '+video_filepath+' -c copy -map 0 -segment_time 00:20:00 -f segment -reset_timestamps 1 '+segment_folder+'/segments_%08d'+video_filepath[-4:]
    try:
        os.system(segment_audio)
    except:
        print('Error during chunking big file')
    return segment_folder




def parse_speech_segments(segmentation_results):
    json_ret=[]
    for item in segmentation_results:
        label = item[0]
        if label=='male' or label=='female':
            start_time = item[1]
            end_time = item[2]
            diff = end_time-start_time
            if diff>=20.0:
                print('Segments are longer than 20sec!! performing finer segmentation')
                no_segments = int(np.ceil(diff/20.0))
                for i in range(no_segments):
                    if i==0:
                        new_start = start_time
                        new_end =start_time+(i*20)+20
                        data_frame={}
                        data_frame['start_time'] = new_start
                        data_frame['end_time'] = new_end
                        data_frame['label'] = 'speech'
                        json_ret.append(data_frame)
                    elif i==(no_segments-1):
                        new_start = new_end
                        new_end = end_time
                        data_frame={}
                        data_frame['start_time'] = new_start
                        data_frame['end_time'] = new_end
                        data_frame['label'] = 'speech'
                        json_ret.append(data_frame)
                    else:
                        new_start = new_end
                        new_end=new_end+20.0
                        data_frame={}
                        data_frame['start_time'] = new_start
                        data_frame['end_time'] = new_end
                        data_frame['label'] = 'speech'
                        json_ret.append(data_frame)
            else:
                data_frame={}
                data_frame['start_time'] = start_time
                data_frame['end_time'] = end_time
                data_frame['label'] = 'speech'
                json_ret.append(data_frame)
    return json_ret





    
    
    
    
    
    
